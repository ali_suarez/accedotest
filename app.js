var mvodApp = angular.module('mvod', [
    'ui.router',
    'ngMaterial',
    'ngSanitize',
    'com.2fdevs.videogular',
    'com.2fdevs.videogular.plugins.controls',
    'com.2fdevs.videogular.plugins.overlayplay',
    'com.2fdevs.videogular.plugins.poster',
    'LocalStorageModule',
    'angular-kaarousel'
]);

mvodApp.run(['$rootScope', '$state',
    function($rootScope, $state)
    {
        $rootScope.$state = $state;
    }
]);

mvodApp.factory('_', ['$window', function($window)
    {
        return $window._;
    }]);

mvodApp.factory('HistoryService', ['localStorageService', '_', function(localStorageService, _)
    {
        var movies = localStorageService.get('movies') || [];
        var factory = {};

        factory.addMovie = function(movie)
        {
            var date = new Date();
            var obj = {
                'date': date.toISOString()
            };
            _.assign(obj, movie);
            movies.push(obj);
            localStorageService.set('movies', movies);
            return localStorageService.get("movies");
        };
        factory.getMovies = function()
        {
            return localStorageService.get("movies");
        };
        factory.getMoviesCount = function()
        {
            if(localStorageService.get("movies") !== null){
                return localStorageService.get("movies").length;
            } else {
                return 0;
            }
        };
        return factory;
    }]);

mvodApp.config(function($stateProvider, $urlRouterProvider, localStorageServiceProvider)
{

    localStorageServiceProvider.setPrefix('mvod');

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('home',
        {
            url: '/',
            templateUrl: 'pages/home.html',
            resolve:
            {
                movies: function($http)
                {
                    return $http(
                    {
                        method: 'GET',
                        url: 'https://demo2697834.mockable.io/movies'
                    }).then(function successCallback(response)
                    {
                        return (response);
                    }, function errorCallback(response)
                    {
                        console.log(response);
                    });
                }
            },
            controller: function($scope, $rootScope, movies, $mdDialog, _, HistoryService, $window)
            {
                $scope.conf = {
                    displayed: 4,
                    perSlide: 2,
                    autoplay: true,
                    direction: 'horizontal',
                    pauseOnHover: true,
                    centerActive: false,
                    timeInterval: 3000,
                    transitionDuration: 500,
                    stopAfterAction: false,
                    hideNav: false,
                    hidePager: false,
                    navOnHover: false,
                    pagerOnHover: false,
                    swipable: true,
                    sync: null,
                    animation: 'slide',
                    loop: true,
                    beforeSlide: function()
                    {
                        //console.log('before slide callback');
                    },
                    afterSlide: function()
                    {
                        //console.log('after slide callback');
                    },
                    minWidth: 320,
                    expand: true,
                    alwaysFill: true,
                    swipeThreshold: 100
                };

                $rootScope.historyItems = HistoryService.getMoviesCount();
                $scope.movies = movies.data.entries;
                $scope.showVideo = function(id, ev)
                {
                    var movie = _.find($scope.movies, function(o)
                    {
                        return o.id == id;
                    });

                    $mdDialog.show(
                        {
                            templateUrl: 'pages/video.html',
                            locals:
                            {
                                movie: movie
                            },
                            parent: angular.element(document.body),
                            targetEvent: ev,
                            clickOutsideToClose: true,
                            fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                            controller: ['$scope', '$sce', '$mdDialog', 'movie', 'HistoryService', function($scope, $sce, $mdDialog, movie, HistoryService)
                                {
                                    $scope.onPlayerReady = function(API)
                                    {
                                        $scope.API = API;
                                    };
                                    $scope.onComplete = function()
                                    {
                                        $scope.close();
                                    };

                                    $scope.onChange = function($state)
                                    {
                                        if ($state == 'play')
                                        {
                                            HistoryService.addMovie(movie);
                                        }
                                    };

                                    $scope.config = {
                                        sources: [
                                            {
                                                src: $sce.trustAsResourceUrl(movie.contents[0].url),
                                                type: "video/" + movie.contents[0].format
                                            }
				                        ],
                                        tracks: [],
                                        theme: "bower_components/videogular-themes-default/videogular.css",
                                        plugins:
                                        {
                                            poster: $sce.trustAsResourceUrl(movie.images[0].url)
                                        }
                                    };
                                    $scope.title = movie.title;
                                    $scope.videoWidth = movie.contents[0].width;
                                    $scope.videoHeight = movie.contents[0].height;
                                    $scope.hide = function()
                                    {
                                        $scope.API.stop();
                                        $scope.API.clearMedia();
                                        $mdDialog.hide();
                                    };

                                    $scope.cancel = function()
                                    {
                                        $scope.API.stop();
                                        $scope.API.clearMedia();
                                        $mdDialog.cancel();
                                    };

                                    $scope.close = function()
                                    {
                                        $scope.API.stop();
                                        $scope.API.clearMedia();
                                        $mdDialog.hide();
                                    };
                                }]
                        })
                        .then(function(answer)
                        {
                            $scope.status = 'You said the information was '
                            ' + answer + '
                            '.';
                        }, function()
                        {
                            $scope.status = 'You cancelled the dialog.';
                        });
                };
            }
        })
        .state('history',
        {
            url: '/history',
            templateUrl: 'pages/history.html',
            controller: function($scope, HistoryService, $rootScope)
            {
                $rootScope.historyItems = HistoryService.getMoviesCount();
                $scope.movies = HistoryService.getMovies();
            }
        });
});